package com.nonononoki.alovoa.config;

public interface DonateConfigurationEnabledIntf {
    String getDonateEnabled();
}
