package com.nonononoki.alovoa.config;

public interface ChatConfigurationEnabledIntf {
    String getChatEnabled();
}
