package com.nonononoki.alovoa.config;

public interface ChatConfigurationUrlIntf {
    String getChatUrl();
}
