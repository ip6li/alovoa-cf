package com.nonononoki.alovoa.repo;

import com.nonononoki.alovoa.entity.user.UserImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserImageRepository extends JpaRepository<UserImage, Long> {
}

